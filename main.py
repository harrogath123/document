import pandas as pd
import openpyxl
import sqlite3
import mysql.connector
import numpy as np
from datetime import datetime
import re


nom_du_fichier = "Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx"
#nom_du_fichier = "test.xlsx"

df = pd.read_excel(nom_du_fichier, sheet_name="Onglet 2 - global", header=3)
df_referenciel = pd.read_excel(nom_du_fichier, sheet_name="Onglet 3 - référentiel NPEC",header=3)
df_referenciel.rename(columns={'Certificateur* \n(*liste non exhaustive - premier dans la fiche RNCP)':'certif'}, inplace=True)
df_referenciel.rename(columns={'Date d\'applicabilité des NPEC** \n(**hors contrats couverts par la valeur d\'amorçage + voir onglet "Me lire")':'date'}, inplace=True)
print(df_referenciel.columns)
df_IDCC = pd.read_excel(nom_du_fichier, sheet_name="Onglet 4 - CPNE-IDCC",header=2)

# np.isnan(df)

# df.head(10)
# referenciel.head(10)
# df_IDCC.head(10)
# df.fillna(0)
#df=df.drop(index=0)
#print(df)
print(df_referenciel)
#print(df_IDCC)

def get_cursor(dictionary=False):
    connection = mysql.connector.connect(user="benjamin", password="coucou123", host="localhost", database="documents")
    return connection, connection.cursor(dictionary=dictionary)

connection, cursor = get_cursor()

codes=['diplome']

#liste_des_rncp= df['Code RNCP'].unique()
#for rncp in liste_des_rncp:
    
   #cursor.execute("""insert into RNCP(Code) values (%s);""", params=(rncp,)) 

#liste_des_certif= df['Intitulé de la certification'].unique()
#for certif in liste_des_certif:
    
    #cursor.execute("""insert into RNCP(Libelle) values (%s);""", params=(certif,)) 

#liste_certif= df['Libellé du diplôme/titre'].unique()
#for certi in liste_certif:
    
    #cursor.execute("""insert into RNCP(diplome) values (%s);""", params=(certi,)) 

liste_des_rncp= df_referenciel['Code RNCP']
for rncp in liste_des_rncp:
   rncp_number = re.search(r'\d+', rncp).group()
   rncp_number = int(rncp_number)
cursor.execute("""insert into rncp(Code,Libelle,certificateur_certif,diplome) values (%s,%s,%s,%s);""", params=(rncp,rncp,rncp_number,rncp)) 
   
liste_libelles= df_referenciel['Libellé de la formation']
for lib in liste_libelles:
    
   cursor.execute("""insert into rncp(Code,Libelle,certificateur_certif,diplome) values (%s,%s,%s,%s);""", params=(lib,lib,lib,lib)) 

liste_certif= df_referenciel['certif']
for certi in liste_certif:
    
   cursor.execute("""insert into certificateur(certif,diplome,CNPE) values (%s,%s,%s);""", params=(certi,certi,certi)) 

liste_certi= df_referenciel['Libellé du Diplôme']
for cert in liste_certi:
    
  cursor.execute("""insert into certificateur(diplome,CNPE,certif) values (%s,%s,%s);""", params=(cert,cert,cert)) 

liste_code = df_referenciel['Code CPNE']
for code in liste_code:
    cursor.execute("""INSERT INTO certificateur(CNPE,diplome,certif) VALUES (%s,%s,%s);""", (code,code,code))

liste_cpne = df_referenciel['CPNE']
for cpne in liste_cpne:
    cursor.execute("""INSERT INTO certificateur(CNPE,diplome,certif) VALUES (%s,%s,%s);""", (cpne,cpne,cpne))

liste_npec = df_referenciel['NPEC final']
for npec in liste_npec:
    cursor.execute("""INSERT INTO idcc(NPEC,statut) VALUES (%s,%s);""", (npec,npec))

liste_statut = df_referenciel['Statut']
for statut in liste_statut:
   cursor.execute("""INSERT INTO idcc(NPEC,statut) VALUES (%s,%s);""", (statut,statut))

liste_date = df_referenciel['date']
for date in liste_date:
    cursor.execute("""INSERT INTO idcc(NPEC,statut,date) VALUES (%s,%s,%s);""", (date,date,date))

connection.commit()
connection.close()
