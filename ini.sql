DROP DATABASE IF EXISTS documents;
CREATE DATABASE documents;
USE documents;

CREATE TABLE `certificateur` (
  `id` int auto_increment PRIMARY KEY,
  `certif` varchar(255) NOT NULL,
  `diplome` varchar(255) NOT NULL,
  `CNPE` varchar(255) NOT NULL,
  INDEX (`certif`)
);

CREATE TABLE `rncp` (
  `id` int auto_increment PRIMARY KEY UNIQUE,
  `Code` varchar(750) NOT NULL,
  `Libelle` varchar(750) NOT NULL,
  `certificateur_certif` varchar(750) NOT NULL,  
  `diplome` varchar(750) NOT NULL
);

CREATE TABLE `idcc` (
  `id` int auto_increment PRIMARY KEY,
  `Code` int,
  `CPNE` int,
  `IDCC` int
);

ALTER TABLE idcc
ADD COLUMN NPEC varchar(255) NOT NULL;

ALTER TABLE idcc
ADD COLUMN statut varchar(255) NOT NULL;

ALTER TABLE idcc
ADD COLUMN date DATE;
