DROP DATABASE IF EXISTS document;
CREATE DATABASE document;
use document;

CREATE TABLE `certificateur` (
  `id` int auto_increment PRIMARY KEY,
  `certif` varchar(255),
  `diplome` varchar(255) not null,
  `CNPE` varchar(255) not null,
  `CNPEFP` int,
  `CPNE_FP` int,
  `CPNEF` int,
  `CPNE` int,
  `COPANIEF` int,
  `CPN_ADITIO` int,
  `CPB` int,
  `Commission_paritaire` int,
  `Conseil` int,
  INDEX (`certif`)
);

CREATE TABLE `RNCP` (
  `id` int auto_increment PRIMARY KEY UNIQUE,
  `Code` varchar(255) not null,
  `Libelle` varchar(750) not null,
  `certificateur_id` int not null,  
  `diplome` varchar(255) not null,
  FOREIGN KEY (`certificateur_id`) REFERENCES `certificateur` (id) 
);

CREATE TABLE `IDCC` (
  `id` int auto_increment PRIMARY KEY,
  `Code` int,
  `CPNE` int,
  `IDCC` int
);

ALTER TABLE IDCC
ADD COLUMN NPEC varchar(255) NOT NULL;

ALTER TABLE IDCC
ADD COLUMN statut varchar(255) NOT NULL;

ALTER TABLE IDCC
ADD COLUMN date DATE;








